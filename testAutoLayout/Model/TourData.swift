//
//  tourData.swift
//  testAutoLayout
//
//  Created by Apple on 11/11/21.
//

import Foundation

struct TourData: Codable {
    
    var tourImage = ""
    var tittle = ""
    var detail = ""
    var numberRating = 0
    
    init(tourImage: String, tittle: String, detail: String, numberRating: Int) {
        self.tourImage = tourImage
        self.tittle = tittle
        self.detail = detail
        self.numberRating = numberRating
    }
    
}
