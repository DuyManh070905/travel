//
//  ViewController.swift
//  testAutoLayout
//
//  Created by Apple on 11/9/21.
//

import UIKit

class ViewController: UIViewController {
    
    let backgroundImageView = UIImageView()
    let loginLabel = UILabel()
    let usernameTextField = UITextField()
    let passwordTextField = UITextField()
    let signinButton = UIButton()
    let forgetPasswordButton = UIButton()
    let viewFirst = UIView()
    let viewSecond = UIView()
    let orLabel = UILabel()
    let createAccoutButton = UIButton()
    var topLoginLabel: CGFloat = 0.0
    var portrait: [NSLayoutConstraint]?
    var landscape: [NSLayoutConstraint]?
    var isPortrait: Bool = false
    lazy var contentViewSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.backgroundColor = .white
        view.frame = self.view.bounds
        view.contentSize = contentViewSize
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScrollView()
        setBackground()
        topLoginLabel = view.frame.height/3
        setLogninLabel()
        setusernameTextField()
        setpasswordTextField()
        setForgetPasswordButton()
        setSigninButton()
        setOrLabel()
        setCreateAccountButton()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        isPortrait = UIDevice.current.orientation.isPortrait
        if isPortrait {
            NSLayoutConstraint.deactivate(landscape!)
            NSLayoutConstraint.activate(portrait!)
        } else {
            NSLayoutConstraint.deactivate(portrait!)
            NSLayoutConstraint.activate(landscape!)
        }
        loginLabel.layoutIfNeeded()
    }
    
    func setBackground() {
        backgroundImageView.image = UIImage(named: "background")
        backgroundImageView.contentMode = .scaleToFill
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
          ])
    }
    
    func setLogninLabel() {
        loginLabel.text = "Login"
        loginLabel.font = loginLabel.font.withSize(30)
        loginLabel.translatesAutoresizingMaskIntoConstraints = false
        portrait = [loginLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 280)]
        landscape = [loginLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 138)]
        
        NSLayoutConstraint.activate([
            loginLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: topLoginLabel),
            loginLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            loginLabel.trailingAnchor.constraint(equalTo: backgroundImageView.trailingAnchor, constant: -10),
            loginLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func setusernameTextField() {
        usernameTextField.placeholder = "Enter your username or number phone"
        usernameTextField.borderStyle = .roundedRect
        usernameTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            usernameTextField.leadingAnchor.constraint(equalTo: loginLabel.leadingAnchor),
            usernameTextField.trailingAnchor.constraint(equalTo: loginLabel.trailingAnchor),
            usernameTextField.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: 30),
            usernameTextField.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    func setpasswordTextField() {
        passwordTextField.placeholder = "Enter your password"
        passwordTextField.borderStyle = .roundedRect
        passwordTextField.isSecureTextEntry = true
        passwordTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            passwordTextField.leadingAnchor.constraint(equalTo: usernameTextField.leadingAnchor),
            passwordTextField.trailingAnchor.constraint(equalTo: usernameTextField.trailingAnchor),
            passwordTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: 30),
            passwordTextField.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    func setForgetPasswordButton() {
        forgetPasswordButton.setTitle("Forget Password?", for: .normal)
        forgetPasswordButton.setTitleColor(.black, for: .normal)
        forgetPasswordButton.contentHorizontalAlignment = .left
        forgetPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            forgetPasswordButton.leadingAnchor.constraint(equalTo: passwordTextField.leadingAnchor),
            forgetPasswordButton.trailingAnchor.constraint(equalTo: passwordTextField.trailingAnchor),
            forgetPasswordButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 30),
            forgetPasswordButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func setSigninButton() {
        signinButton.setTitle("Sign in", for: .normal)
        signinButton.setTitleColor(.white, for: .normal)
        signinButton.contentHorizontalAlignment = .center
        signinButton.backgroundColor = UIColor(red: 42/255, green: 165/255, blue: 87/255, alpha: 1)
        signinButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            signinButton.leadingAnchor.constraint(equalTo: forgetPasswordButton.leadingAnchor),
            signinButton.trailingAnchor.constraint(equalTo: forgetPasswordButton.trailingAnchor),
            signinButton.topAnchor.constraint(equalTo: forgetPasswordButton.bottomAnchor, constant: 20),
            signinButton.heightAnchor.constraint(equalTo: forgetPasswordButton.heightAnchor)
        ])
        signinButton.addTarget(self, action: #selector(ClickSigninButton), for: .touchUpInside)
    }
    
    @objc func ClickSigninButton() {
        if usernameTextField.text == "" {
            self.view.makeToast("Enter your username")
            return
        }
        if passwordTextField.text == "" {
            self.view.makeToast("Enter your password")
            return
        }
        self.view.makeToast("Login success")
        let viewControllection = ExploreViewController(nibName: "ExploreViewController", bundle: nil)
        self.navigationController?.pushViewController(viewControllection, animated: true)
    }
    
    func setOrLabel() {
        orLabel.text = "or"
        orLabel.textColor = .black
        orLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            orLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            orLabel.topAnchor.constraint(equalTo: signinButton.bottomAnchor, constant: 20),
            orLabel.heightAnchor.constraint(equalToConstant: 25),
            orLabel.widthAnchor.constraint(equalToConstant: 20)
        ])
    }
    
    func setCreateAccountButton() {
        createAccoutButton.setTitle("Create An Account", for: .normal)
        createAccoutButton.setTitleColor(.white, for: .normal)
        createAccoutButton.contentHorizontalAlignment = .center
        createAccoutButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        createAccoutButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            createAccoutButton.leadingAnchor.constraint(equalTo: signinButton.leadingAnchor),
            createAccoutButton.trailingAnchor.constraint(equalTo: signinButton.trailingAnchor),
            createAccoutButton.topAnchor.constraint(equalTo: orLabel.bottomAnchor, constant: 20),
            createAccoutButton.heightAnchor.constraint(equalTo: signinButton.heightAnchor)
        ])
        createAccoutButton.addTarget(self, action: #selector(clickCreateAccount), for: .touchUpInside)
    }
    
    @objc func clickCreateAccount() {
        let viewController = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setScrollView() {
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.addSubview(backgroundImageView)
        scrollView.addSubview(loginLabel)
        scrollView.addSubview(usernameTextField)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(forgetPasswordButton)
        scrollView.addSubview(signinButton)
        scrollView.addSubview(orLabel)
        scrollView.addSubview(createAccoutButton)
    }
    
}

