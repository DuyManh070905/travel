//
//  RegisterViewController.swift
//  testAutoLayout
//
//  Created by Apple on 11/10/21.
//

import UIKit
import Toast_Swift

class RegisterViewController: UIViewController {
    
    let backgroundImage = UIImageView()
    let registerLabel = UILabel()
    let fullnameTextfield = UITextField()
    let birthdayTexxtField = UITextField()
    let sexTextField = UITextField()
    let addressTextField = UITextField()
    let avatarButton = UIButton()
    let avatarImage = UIImageView()
    let signupButton = UIButton()
    let sexSegmentedControl = UISegmentedControl(items: ["Male", "Female"])
    let birthdayDatePicker = UIDatePicker()
    var topLoginLabel: CGFloat = 0.0
    var portrait: [NSLayoutConstraint]?
    var landscape: [NSLayoutConstraint]?
    var isPortrait: Bool = false
    lazy var contentViewSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
    lazy var scrollView: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.backgroundColor = .white
        view.frame = self.view.bounds
        view.contentSize = contentViewSize
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScrollView()
        setBackgroundImage()
        topLoginLabel = view.frame.height/3
        setRegisterLabel()
        setFullnameTextField()
        setBirthdayTextField()
        setSexTextField()
        setAddressTextField()
        setAvatarButton()
        setAvatarImage()
        setSignupButton()
        setSexSegmentedControl()
        setBirthdayDatePicker()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        isPortrait = UIDevice.current.orientation.isPortrait
        if isPortrait {
            NSLayoutConstraint.deactivate(landscape!)
            NSLayoutConstraint.activate(portrait!)
        } else {
            NSLayoutConstraint.deactivate(portrait!)
            NSLayoutConstraint.activate(landscape!)
        }
        registerLabel.layoutIfNeeded()
    }
    
    func setBackgroundImage() {
        backgroundImage.image = UIImage(named: "background")
        backgroundImage.contentMode = .scaleToFill
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor)
          ])
    }
    
    func setRegisterLabel(){
        registerLabel.text = "Register"
        registerLabel.font = registerLabel.font.withSize(30)
        registerLabel.translatesAutoresizingMaskIntoConstraints = false
        portrait = [registerLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 225)]
        landscape = [registerLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 88)]
        
        NSLayoutConstraint.activate([
            registerLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 225),
            registerLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 10),
            registerLabel.trailingAnchor.constraint(equalTo: backgroundImage.trailingAnchor, constant: -10),
            registerLabel.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func setFullnameTextField() {
        fullnameTextfield.placeholder = "Full name"
        fullnameTextfield.borderStyle = .roundedRect
        fullnameTextfield.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            fullnameTextfield.leadingAnchor.constraint(equalTo: registerLabel.leadingAnchor),
            fullnameTextfield.trailingAnchor.constraint(equalTo: registerLabel.trailingAnchor),
            fullnameTextfield.topAnchor.constraint(equalTo: registerLabel.bottomAnchor, constant: 25),
            fullnameTextfield.heightAnchor.constraint(equalToConstant: 35)
            
        ])
    }
    
    func setBirthdayTextField() {
        birthdayTexxtField.placeholder = "Birth day"
        birthdayTexxtField.borderStyle = .roundedRect
        birthdayDatePicker.isHidden = true
        sexTextField.isEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(textFieldEditing))
        self.birthdayTexxtField.addGestureRecognizer(tap)
        birthdayTexxtField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            birthdayTexxtField.leadingAnchor.constraint(equalTo: fullnameTextfield.leadingAnchor),
            birthdayTexxtField.trailingAnchor.constraint(equalTo: fullnameTextfield.trailingAnchor),
            birthdayTexxtField.topAnchor.constraint(equalTo: fullnameTextfield.bottomAnchor, constant: 15),
            birthdayTexxtField.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    @objc func textFieldEditing() {
        self.birthdayTexxtField.resignFirstResponder()
        birthdayDatePicker.isHidden = false
    }
    
    func setSexTextField() {
        sexTextField.placeholder = "Sex"
        sexTextField.borderStyle = .roundedRect
        sexTextField.isEnabled = false
        sexTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sexTextField.leadingAnchor.constraint(equalTo: birthdayTexxtField.leadingAnchor),
            sexTextField.trailingAnchor.constraint(equalTo: birthdayTexxtField.trailingAnchor),
            sexTextField.topAnchor.constraint(equalTo: birthdayTexxtField.bottomAnchor, constant: 15),
            sexTextField.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    func setAddressTextField() {
        addressTextField.placeholder = "Address"
        addressTextField.borderStyle = .roundedRect
        addressTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addressTextField.leadingAnchor.constraint(equalTo: sexTextField.leadingAnchor),
            addressTextField.trailingAnchor.constraint(equalTo: sexTextField.trailingAnchor),
            addressTextField.topAnchor.constraint(equalTo: sexTextField.bottomAnchor, constant: 15),
            addressTextField.heightAnchor.constraint(equalToConstant: 35)
        ])
    }
    
    func setAvatarButton() {
        avatarButton.setTitle("Choose avatar", for: .normal)
        avatarButton.setTitleColor(.blue, for: .normal)
        avatarButton.contentHorizontalAlignment = .left
        avatarButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            avatarButton.leadingAnchor.constraint(equalTo: addressTextField.leadingAnchor),
            avatarButton.trailingAnchor.constraint(equalTo: addressTextField.trailingAnchor),
            avatarButton.topAnchor.constraint(equalTo: addressTextField.bottomAnchor, constant: 15),
            avatarButton.heightAnchor.constraint(equalToConstant: 50)
        ])
        avatarButton.addTarget(self, action: #selector(tapOnAvatarImageView), for: .touchUpInside)
    }
    
    func setAvatarImage() {
        avatarImage.image = UIImage(named: "user")
        avatarImage.layer.borderWidth = 1
        avatarImage.contentMode = .scaleToFill
        avatarImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            avatarImage.heightAnchor.constraint(equalToConstant: 50),
            avatarImage.widthAnchor.constraint(equalToConstant: 50),
            avatarImage.topAnchor.constraint(equalTo: addressTextField.bottomAnchor, constant: 15),
            avatarImage.trailingAnchor.constraint(equalTo: avatarButton.trailingAnchor)
        ])
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.addTarget(self, action: #selector(tapOnAvatarImageView))
        avatarImage.addGestureRecognizer(tapGestureRecognizer)
        avatarImage.isUserInteractionEnabled = true
    }
    
    @objc func tapOnAvatarImageView() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func setSignupButton() {
        signupButton.setTitle("Sign up", for: .normal)
        signupButton.setTitleColor(.white, for: .normal)
        signupButton.contentHorizontalAlignment = .center
        signupButton.backgroundColor = UIColor(red: 42/255, green: 165/255, blue: 87/255, alpha: 1)
        signupButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            signupButton.leadingAnchor.constraint(equalTo: avatarButton.leadingAnchor),
            signupButton.trailingAnchor.constraint(equalTo: avatarButton.trailingAnchor),
            signupButton.topAnchor.constraint(equalTo: avatarButton.bottomAnchor, constant: 20),
            signupButton.heightAnchor.constraint(equalToConstant: 40)
        ])
        signupButton.addTarget(self, action: #selector(clickSigninButton), for: .touchUpInside)
    }
    
    @objc func clickSigninButton() {
        if fullnameTextfield.text == "" {
            self.view.makeToast("Enter your full name")
            return
        }
        if birthdayTexxtField.text == "" {
            self.view.makeToast("Choose your birthday")
            return
        }
        if sexTextField.text == "" {
            self.view.makeToast("Choose your sex")
            return
        }
        if addressTextField.text == "" {
            self.view.makeToast("Enter your address")
            return
        }
        self.navigationController?.popViewController(animated: true)
        self.view.makeToast("Register Success")
    }
    
    func setSexSegmentedControl() {
        sexSegmentedControl.selectedSegmentIndex = 0
        sexSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sexSegmentedControl.topAnchor.constraint(equalTo: sexTextField.topAnchor),
            sexSegmentedControl.bottomAnchor.constraint(equalTo: sexTextField.bottomAnchor),
            sexSegmentedControl.trailingAnchor.constraint(equalTo: sexTextField.trailingAnchor),
            sexSegmentedControl.widthAnchor.constraint(equalToConstant: 130)
        ])
        sexSegmentedControl.addTarget(self, action: #selector(clickChooseSex), for: .valueChanged)
    }
    
    @objc func clickChooseSex() {
        switch sexSegmentedControl.selectedSegmentIndex {
        case 0:
            sexTextField.text = sexSegmentedControl.titleForSegment(at: 0)
        case 1:
            sexTextField.text = sexSegmentedControl.titleForSegment(at: 1)
        default:
            break;
        }
    }
    
    func setBirthdayDatePicker() {
        birthdayDatePicker.datePickerMode = .date
        birthdayDatePicker.preferredDatePickerStyle = .wheels
        birthdayDatePicker.backgroundColor = .white
        birthdayDatePicker.isHidden = true
        birthdayDatePicker.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            birthdayDatePicker.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            birthdayDatePicker.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            birthdayDatePicker.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            birthdayDatePicker.heightAnchor.constraint(equalToConstant: 200)
        ])
        birthdayDatePicker.addTarget(self, action: #selector(chooseBirthDayDatePicker), for: .valueChanged)
    }
    
    @objc func chooseBirthDayDatePicker() {
        let date = DateFormatter()
        date.dateFormat = "dd-MM-yyyy"
        birthdayTexxtField.text = date.string(from: birthdayDatePicker.date)
        birthdayDatePicker.isHidden = true
    }
    
    func setScrollView() {
        let frame = self.view.safeAreaLayoutGuide.layoutFrame
        self.view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.addSubview(backgroundImage)
        scrollView.addSubview(registerLabel)
        scrollView.addSubview(fullnameTextfield)
        scrollView.addSubview(birthdayTexxtField)
        scrollView.addSubview(sexTextField)
        scrollView.addSubview(addressTextField)
        scrollView.addSubview(avatarButton)
        scrollView.addSubview(avatarImage)
        scrollView.addSubview(signupButton)
        scrollView.addSubview(sexSegmentedControl)
        scrollView.addSubview(birthdayDatePicker)
    }
    
}

extension RegisterViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            avatarImage.image = image
            print()
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension RegisterViewController: UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
