//
//  CellCollectionView.swift
//  testAutoLayout
//
//  Created by Apple on 11/11/21.
//

import Foundation
import UIKit

protocol PassingDataDelegate
{
    func passNumberStar(number: Int, titlle: String)
}

class CustomCell: UICollectionViewCell {
    
    let star1Button = UIButton()
    let star2Button = UIButton()
    let star3Button = UIButton()
    let star4Button = UIButton()
    let star5Button = UIButton()
    var numberStar = 0
    var tittle = ""
    var height:CGFloat = 280
    var delegate: PassingDataDelegate?
    
    fileprivate let tourImageView: UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleToFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 12
        return image
    }()
    
    fileprivate let titlleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "Helvetica Neue", size: 18)
        label.textAlignment = .left
        return label
    }()
    
    fileprivate let detailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "System", size: 13)
        label.textAlignment = .left
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setTourImage()
        setTitllalabel()
        setDetaillabel()
        setRatingStackView()
        setStarButton(number: numberStar)
        setStar1Button()
        setStar2Button()
        setStar3Button()
        setStar4Button()
        setStar5Button()
        self.height = tourImageView.frame.height + titlleLabel.frame.height + detailLabel.frame.height
    }
    
    func setTourImage(){
        contentView.addSubview(tourImageView)
        NSLayoutConstraint.activate([
            tourImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            tourImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            tourImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            tourImageView.heightAnchor.constraint(equalToConstant: 200)
        ])
    }
    
    func setTitllalabel(){
        contentView.addSubview(titlleLabel)
        NSLayoutConstraint.activate([
            titlleLabel.topAnchor.constraint(equalTo: tourImageView.bottomAnchor),
            titlleLabel.leadingAnchor.constraint(equalTo: tourImageView.leadingAnchor),
            titlleLabel.trailingAnchor.constraint(equalTo: tourImageView.trailingAnchor)
        ])
    }
    
    func setDetaillabel(){
        contentView.addSubview(detailLabel)
        NSLayoutConstraint.activate([
            detailLabel.topAnchor.constraint(equalTo: titlleLabel.bottomAnchor),
            detailLabel.leadingAnchor.constraint(equalTo: titlleLabel.leadingAnchor),
            detailLabel.trailingAnchor.constraint(equalTo: titlleLabel.trailingAnchor)
        ])
    }
    
    func setRatingStackView() {
        let stackView = UIStackView(arrangedSubviews: [star1Button, star2Button, star3Button, star4Button, star5Button])
        stackView.axis = .horizontal
        stackView.spacing = 0
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: detailLabel.bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: detailLabel.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: detailLabel.trailingAnchor),
            stackView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func setStar1Button() {
        star1Button.setImage(UIImage(systemName: "star"), for: .normal)
        star1Button.tintColor = .systemYellow
        star1Button.addTarget(self, action: #selector(clickStar1Button), for: .touchUpInside)
    }
    
    @objc func clickStar1Button() {
        numberStar = 1
        setStarButton(number: numberStar)
        if let tittle = titlleLabel.text {
            delegate?.passNumberStar(number: numberStar, titlle: tittle)
        }
        
    }
    
    func setStar2Button() {
        star2Button.setImage(UIImage(systemName: "star"), for: .normal)
        star2Button.tintColor = .systemYellow
        star2Button.addTarget(self, action: #selector(clickStar2Button), for: .touchUpInside)
    }
    
    @objc func clickStar2Button() {
        numberStar = 2
        setStarButton(number: numberStar)
        if let tittle = titlleLabel.text {
            delegate?.passNumberStar(number: numberStar, titlle: tittle)
        }
    }
    
    func setStar3Button() {
        star3Button.setImage(UIImage(systemName: "star"), for: .normal)
        star3Button.tintColor = .systemYellow
        star3Button.addTarget(self, action: #selector(clickStar3Button), for: .touchUpInside)
    }
    
    @objc func clickStar3Button() {
        numberStar = 3
        setStarButton(number: numberStar)
        if let tittle = titlleLabel.text {
            delegate?.passNumberStar(number: numberStar, titlle: tittle)
        }
    }
    
    func setStar4Button() {
        star4Button.setImage(UIImage(systemName: "star"), for: .normal)
        star4Button.tintColor = .systemYellow
        star4Button.addTarget(self, action: #selector(clickStar4Button), for: .touchUpInside)
    }
    
    @objc func clickStar4Button() {
        numberStar = 4
        setStarButton(number: numberStar)
        if let tittle = titlleLabel.text {
            delegate?.passNumberStar(number: numberStar, titlle: tittle)
        }
    }
    
    func setStar5Button() {
        star5Button.setImage(UIImage(systemName: "star"), for: .normal)
        star5Button.tintColor = .systemYellow
        star5Button.addTarget(self, action: #selector(clickStar5Button), for: .touchUpInside)
    }
    
    @objc func clickStar5Button() {
        numberStar = 5
        setStarButton(number: numberStar)
        if let tittle = titlleLabel.text {
            delegate?.passNumberStar(number: numberStar, titlle: tittle)
        }
    }
    
    func setStar(numberButton: Int, styleStar: String ) {
        switch numberButton {
            case 1:
                star1Button.setImage(UIImage(systemName: styleStar)!, for: .normal)
            case 2:
                star2Button.setImage(UIImage(systemName: styleStar)!, for: .normal)
            case 3:
                star3Button.setImage(UIImage(systemName: styleStar)!, for: .normal)
            case 4:
                star4Button.setImage(UIImage(systemName: styleStar)!, for: .normal)
            case 5:
                star5Button.setImage(UIImage(systemName: styleStar)!, for: .normal)
            default:
                break
            }
    }
    
    func setStarButton(number: Int) {
        if number == 0 {
            for temp in 1..<6 {
                setStar(numberButton: temp, styleStar: "star")
            }
        } else {
            let starNumberClick = number + 1
            for temp in 1..<starNumberClick {
                setStar(numberButton: temp, styleStar: "star.fill")
            }
            for temp in starNumberClick..<6 {
                setStar(numberButton: temp, styleStar: "star")
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomCell {
    
    func setData(_ data: TourData) {
        tourImageView.image = UIImage(named: data.tourImage)
        titlleLabel.text = data.tittle
        detailLabel.text = data.detail
        numberStar = data.numberRating
    }
}
