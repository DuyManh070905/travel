//
//  ExploreViewController.swift
//  testAutoLayout
//
//  Created by Apple on 11/10/21.
//

import UIKit

class ExploreViewController: UIViewController {

    let exploreLabel = UILabel()
    let avatarImage = UIImageView()
    let view1 = UIView()
    let searchBar = UISearchBar()
    let errorFind = UILabel()
    var collectionView: UICollectionView?
    var numberStar = 0
    var titleTemp = ""
    var arrayTour = [TourData]()
    var filteredData = [""]
    var arrayStringTittle = [""]
    var filteredTour = [TourData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setExploreLabel()
        setAvatarImage()
        setView1()
        setSearchBar()
        setCollectionView()
        saveData()
        setData()
        filteredData = arrayStringTittle
        filteredTour = arrayTour
    }
    
    func saveData() {
        self.arrayTour.removeAll()
        if getData().count > 0 {
            self.arrayTour.append(contentsOf: (getData()))
        } else {
            let arrayInit = [TourData(tourImage: "background", tittle: "Moc Chau 1", detail: "Son La1", numberRating: 0), TourData(tourImage: "background2", tittle: "vinh ha long1", detail: "Quang ninh", numberRating: 0), TourData(tourImage: "background", tittle: "Moc Chau2", detail: "Son La", numberRating: 0), TourData(tourImage: "background2", tittle: "vinh ha long2", detail: "Quang ninh", numberRating: 0), TourData(tourImage: "background", tittle: "Moc Chau3", detail: "Son La", numberRating: 0), TourData(tourImage: "background2", tittle: "vinh ha long3", detail: "Quang ninh", numberRating: 0), TourData(tourImage: "background", tittle: "Moc Chau4", detail: "Son La", numberRating: 0), TourData(tourImage: "background2", tittle: "vinh ha long4", detail: "Quang ninh", numberRating: 0), TourData(tourImage: "background", tittle: "Moc Chau5", detail: "Son La", numberRating: 0)]
            if let encoded = try? JSONEncoder().encode(arrayInit) {
                UserDefaults.standard.set(encoded, forKey: "tourKey")
            }
            self.arrayTour.append(contentsOf: arrayInit)
        }
    }
    
    func getData() -> [TourData] {
        if let data = UserDefaults.standard.data(forKey: "tourKey") {
            if let decoded = try? JSONDecoder().decode([TourData].self, from: data) {
                return decoded
            }
        }

        return []
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setData() {
        for tour in arrayTour {
            arrayStringTittle.append(tour.tittle)
        }
        filteredData.removeAll()
        collectionView?.reloadData()
    }
    
    func setExploreLabel() {
        exploreLabel.text = "Explore"
        exploreLabel.font = exploreLabel.font.withSize(30)
        self.view.addSubview(exploreLabel)
        exploreLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            exploreLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 90),
            exploreLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            exploreLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            exploreLabel.heightAnchor.constraint(equalToConstant: 50)
        ])
    }

    func setAvatarImage() {
        avatarImage.image = UIImage(named: "user")
        avatarImage.layer.borderWidth = 1
        avatarImage.contentMode = .scaleToFill
        view.addSubview(avatarImage)
        avatarImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            avatarImage.heightAnchor.constraint(equalTo: exploreLabel.heightAnchor),
            avatarImage.widthAnchor.constraint(equalToConstant: 50),
            avatarImage.topAnchor.constraint(equalTo: exploreLabel.topAnchor),
            avatarImage.trailingAnchor.constraint(equalTo: exploreLabel.trailingAnchor)
        ])
    }

    func setView1() {
        view1.backgroundColor = .systemGray
        view.addSubview(view1)
        view1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view1.topAnchor.constraint(equalTo: exploreLabel.bottomAnchor, constant: 10),
            view1.leadingAnchor.constraint(equalTo: exploreLabel.leadingAnchor),
            view1.trailingAnchor.constraint(equalTo: exploreLabel.trailingAnchor),
            view1.heightAnchor.constraint(equalToConstant: 2)
        ])
    }

    func setSearchBar() {
        searchBar.placeholder = "Where do you want to go?"
        searchBar.delegate = self
        view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view1.bottomAnchor, constant: 10),
            searchBar.leadingAnchor.constraint(equalTo: view1.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: view1.trailingAnchor),
            searchBar.heightAnchor.constraint(equalToConstant: 44)
        ])
    }

    func setCollectionView() {
        let layout = UICollectionViewFlowLayout()
//        let widthCell = (view.frame.width ) / 3
//        layout.itemSize = CGSize(width: widthCell, height: 280)
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        if let collectionView = collectionView {
            self.view.addSubview(collectionView)
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                collectionView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 10),
                collectionView.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
                collectionView.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor),
                collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            collectionView.backgroundColor = .white
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.register(CustomCell.self, forCellWithReuseIdentifier: "cell")
        }
    }

}

extension ExploreViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredTour.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CustomCell else {
            return UICollectionViewCell()
        }
        let data = filteredTour[indexPath.row]
        let value = data.numberRating
        cell.setData(data)
        cell.setStarButton(number: value)
        cell.delegate = self
        return cell
    }
    
}

extension ExploreViewController: PassingDataDelegate {

    func passNumberStar(number: Int, titlle: String) {
        for i in 0..<filteredTour.count {
            let data = filteredTour[i]
            if data.tittle == titlle
            {
                filteredTour[i].numberRating = number
            }
        }
        for i in 0..<arrayTour.count {
            let data = arrayTour[i]
            if data.tittle == titlle
            {
                arrayTour[i].numberRating = number
            }
        }
    }
}

extension ExploreViewController: UICollectionViewDelegateFlowLayout {
  // 1
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    // 2
    let widthCell = (view.frame.width - 60) / 3
    return CGSize(width: widthCell, height: 280)
  }
}

extension ExploreViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredTour.removeAll()
        filteredData = searchText.isEmpty ? arrayStringTittle : arrayStringTittle.filter({(dataString: String) -> Bool in
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
            })
        if filteredData.count == 0 {
            errorFind.text = "Cann't find any place?"
            errorFind.textAlignment = .center
            view.addSubview(errorFind)
            errorFind.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                errorFind.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 20),
                errorFind.leadingAnchor.constraint(equalTo: searchBar.leadingAnchor),
                errorFind.trailingAnchor.constraint(equalTo: searchBar.trailingAnchor)
            ])
        } else {
            errorFind.text = ""
            for tittle in filteredData {
                for tour in arrayTour{
                    if tour.tittle == tittle {
                        filteredTour.append(tour)
                        collectionView?.reloadData()
                    }
                }
            }
        }
        filteredData.removeAll()
        collectionView?.reloadData()
    }
}
